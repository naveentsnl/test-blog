<!DOCTYPE html>
<html>
<head>

	@include('inc/head')

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		@include('inc/header')

		@include('inc/sidebar')

			@yield('content')
			
		@include('inc/footer')
		
	</div>
	@include('inc/fileinclude')
</body>

 <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
        <script>
            CKEDITOR.replace( 'article-ckeditor' );
        </script>
        <script  src="{{ asset('/js/like.js') }}"></script>
        <script type="text/javascript">
            var token = '{{ Session :: token() }}';
            var urlLike = '{{ route('like') }}';
            
         </script>
</html>