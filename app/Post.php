<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //Table Name
    protected $table = 'posts';

    //Primary key
    protected $key = '$id';

    //Timestamp
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function likes(){
    	return $this->belongsTo('App\Like');
    } 
    public function comments(){
        return $this->hasMany('App\Comment');
    }
}
