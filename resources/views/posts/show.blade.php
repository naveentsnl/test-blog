@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-default">Go Back</a>
    <h1>{{$post->title}}</h1>  
    <img style="width:100%" src="/uploads/{{$post->cover_image}}">  
    <div>
        {!!$post->body!!}
        <!-- {{$post->body}} show the content without style like tag. but {!!$post->body!!}  shows the style too-->
    </div>
    <small>
        Written on {{$post->created_at}}
    </small>
    <hr>
    @if (!Auth::guest())
        @if (Auth::user()->id == $post->user_id)
            <a href="/posts/{{$post->id}}/edit" class="btn btn-default">Edit</a>

            {!!Form::open(['action' => ['PostController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                {!!Form::hidden('_method','DELETE')!!}
                {!!Form::submit('delete',['class'=> 'btn btn-danger'])!!}
            {!!Form::close()!!}
            
        @endif
        
        
    @endif

     <div class="interaction">
        <a href="/like/{{ $post->id }}" class="btn btn-primary">Like({{$like_count }})<a> |

        <a href="/dislike/{{ $post->id }}" class="btn btn-primary">Dislike</a> |
                            
        <a href="" class="btn btn-primary"  data-toggle="collapse" data-target="#comments">Comments</a>

        <div class="row collapse pt-2 pl-5" id = "comments"  style="background-color: #0f0f0f0f;">
            
            <div class="interaction">
                           
                <div>
                    <form method = "post" action="/comment/{{$post->id}}">

                        {{ csrf_field() }}
                        {{ method_field('POST') }} 
                        
                        <textarea name="comment" rows=5 cols=120 required></textarea> 
                        
                        <br>
                            
                        <button type = "submit" class="btn btn-success">Comment</button> 

                        <br>

                        </form>

                         @foreach ($comment as $comments)
                
                            <div class="row pt-1 pl-2">
                                
                                <h4>{{ $comments->user->name}}</h4><br><br>
                                {{$comments->comment}}                              

                            </div>
                    
                           <hr>

                        @endforeach
                            
                    

                </div>
                                       
             </div>

        
        </div>

                            
    </div> 
    
@endsection