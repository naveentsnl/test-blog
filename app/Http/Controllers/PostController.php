<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Post;
use App\Like;
use App\Comment;
use App\User;
use DB;
use Socialite;
use \Mail;

use App\Http\Resources\BlogResource;
use App\Http\Requests;


class PostController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**public function __construct()
    {
        $this->middleware('auth', ['except'=> ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$posts = Post:all();
        //$posts = DB::select("SELECT * FROM posts");
        //$posts = Post::orderBy('title','desc')->get();
        //$posts = Post::orderBy('title','desc')->take(1)->get();
        $posts = Post::orderBy('created_at','asc')->paginate(10);
        // return Post::where('title','Post One')->get();

        return view('posts.index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:2999',
        ]);

        //Handle File Upload
        if($request->hasFile('cover_image')){
            //Get file name with extension
            $fileNameWithExt = $request ->file('cover_image')->getClientOriginalName();

            //Get the file name only
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            //Get the file extension only
            $extension = $request->file('cover_image')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;

            //Upload Image

            $path = $request->file('cover_image')->storeAs('/',$fileNameToStore,'public');

        }else{
            $fileNameToStore = 'noimage.jpg';
        }

        //Create Post
        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->api_token = auth()->user()->api_token;
        $post->cover_image = $fileNameToStore;
        $post->save();

        return redirect('/posts')->with('success','Posts Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
         


        //Show the Like, DisLike and Comment

        $like_post = Post::find($id);

        $comment = Comment::where(['post_id' => $id])->orderBy('created_at','asc')->get();

        $like_count = Like::where(['post_id' => $like_post->id])->count();

        return view('posts.show', ['post'=>$post, 'like_count'=>$like_count, 'comment'=>$comment]);



        


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        //check the correct user
        if(auth()->user()->id!==$post->user_id){
           return redirect('/posts')->with('error','Unauthorized Page'); 
        }

        return view('posts.edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:2999',
        ]);


        //Handle File Upload
        if($request->hasFile('cover_image')){
            //Get file name with extension
            $fileNameWithExt = $request ->file('cover_image')->getClientOriginalName();

            //Get the file name only
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            //Get the file extension only
            $extension = $request->file('cover_image')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;

            //Upload Image
            $path = $request->file('cover_image')->storeAs('/',$fileNameToStore,'public');

        }else{
            $fileNameToStore = 'noimage.jpg';
        }

        //Create Post
        $post =Post::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->cover_image = $fileNameToStore;
        $post->save();

        return redirect('/posts')->with('success','Posts Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
         //check the correct user
        //dd(auth()->user());
         if(auth()->user()->id!==$post->user_id){
            return redirect('/posts')->with('error','Unauthorized Page'); 
         }

         //Delete Image
         if($post->cover_image != 'noimage.jpg'){
             Storage::delete('/'.$post->cover_image,'public');
         }

        $post->delete();
        return redirect('/posts')->with('success','Post Removed');
    }

    /**
     * @method likePost()
     * 
     * @uses used to update the like/dislike status
     * 
     * @created Naveen S
     *
     * @updated - 
     *
     * @param integer postId
     *
     * @param boolean isLike
     *
     * @return JSON Response
     */

    public function likePost(Request $request) {

        // Validation

        // validate postID and isLike are required

        // fails - return error json response

        $user_like_details = Like::where('user_id', Auth::user()->id)->where('post_id', $request->postId)->first();

        \Log::info($user_like_details,true);

        if($user_like_details) {

            $user_like_details->like = 0;

            $user_like_details->save();

        } else {

            $user_like_details = new Like;

            $user_like_details->user_id = Auth::user()->id;

            $user_like_details->post_id = $request->postId;

            $user_like_details->like = 1;

            $user_like_details->save();
        }

        $message = $user_like_details->like == 1 ? "Liked" : "Disliked";

        $data = ['post_id' => $request->postId, 'like' => $user_like_details->like];

        $response_array = ['success' => true, 'message' => $message,'data' => $data];

        return response()->json($response_array, 200);

        // END

        if($like){
            $already_like = $like->like;
            if($already_like != $is_like) {
                $like->like = $is_like;
                $update = true;
            }
        }
        else{
            $like = new Like;
        }
        $like->like = $is_like;
        $like->user_id = $user->id;
        $like->post_id = $post->id;

        if($update){
            $like->update();
        }
        else{
            $like->save();
        }

    }



    /**

    * @method like()

    * @param id

    **/
    public function like($id){

        $loggedin_user = Auth::user()->id;

        $like_user = Like::where(['user_id'=>$loggedin_user, 'post_id'=>$id])->first();

        if(empty($like_user->user_id)){

            $user_id = Auth::user()->id;

            $post_id = $id;

            $like = New Like;

            $like->user_id = $user_id;

            $like->post_id = $post_id;

            $like->save();

            return redirect("/posts/{$id}");

        }
        else{

            return redirect("/posts/{$id}");

        }

    }



    /**

    * @method comment()

    * @param id

    **/
    public function comment(Request $request,$id){

        $this->validate($request,[

            'comment' => 'required'
        
        ]);

        $comment = new Comment;

        $comment->comment = $request->input('comment');

        $comment->user_id = Auth::user()->id;

        $comment->post_id = $id;

        $comment->save();

        return redirect("/posts/{$id}");
    
    }

    /**

    * @function email

    * to view the php

    **/

    public function email(){

        $to_name = "Balaji M";

        $to_email = "balajimn27@gmail.com";

        $data = array("name"=>"Balaji", "body" => "You got 1 Rs cash Reward");

        Mail::send("emails.mail", $data, function($message) use ($to_name, $to_email) {$message->to($to_email, $to_name)->subject("Laravel Test Mail");

        $message->from("naveensnlbe@gmail.com","Reward");});

    }

    
    
}






       


        