<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*Route::get('posts', 'PostController@index');

Route::get('post/{id}', 'PostController@show');

Route::post('post', 'PostController@store');

Route::put('post/{id}', 'PostController@update');

Route::delete('post/{id}', 'PostController@destroy');
*/
//Route::post('login', 'API\UserController@login');

//Route::post('register', 'API\UserController@register');

//Route::get('details', 'API\UserController@details');


// Login
Route::post('/login','BasicAuthController@postLogin');

// Register
Route::post('/register','BasicAuthController@postRegister');

// User
Route::get('/users/{apitoken}','BasicAuthController@user');

    // Logout
Route::post('/logout','BasicAuthController@postLogout');

Route::get('/posts','ApiController@index');

Route::get('post/{id}', 'ApiController@show');

Route::post('post', 'ApiController@store');

Route::put('post/{id}', 'ApiController@update');

Route::post('posts/{id}', 'ApiController@destroy');
