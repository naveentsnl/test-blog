<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Post;
use App\Like;
use App\Comment;
use App\User;
use DB;


use App\Http\Resources\BlogResource;
use App\Http\Requests;

class AdminPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $posts = Post::orderBy('created_at','asc')->paginate(10);
        // return Post::where('title','Post One')->get();

        return view('admin.posts.index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:2999',
        ]);

            //Handle File Upload
            if($request->hasFile('cover_image')){
                //Get file name with extension
                $fileNameWithExt = $request ->file('cover_image')->getClientOriginalName();

                //Get the file name only
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                //Get the file extension only
                $extension = $request->file('cover_image')->getClientOriginalExtension();

                //Filename to store
                $fileNameToStore = $fileName.'_'.time().'.'.$extension;

                //Upload Image

                $path = $request->file('cover_image')->storeAs('/',$fileNameToStore,'public');

            }else{
                $fileNameToStore = 'noimage.jpg';
            }

            //Create Post
            $post = new Post;
            $post->title = $request->input('title');
            $post->body = $request->input('body');
            $post->user_id = 0;
            $post->admin_id = 1;
            $post->api_token = auth()->user()->api_token;
            $post->cover_image = $fileNameToStore;
            $post->save();

            return redirect('/admin/posts')->with('success','Posts Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $post = Post::find($id);
      

        return view('admin.posts.show', ['post'=>$post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('admin.posts.edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:2999',
        ]);


        //Handle File Upload
        if($request->hasFile('cover_image')){
            //Get file name with extension
            $fileNameWithExt = $request ->file('cover_image')->getClientOriginalName();

            //Get the file name only
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            //Get the file extension only
            $extension = $request->file('cover_image')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;

            //Upload Image
            $path = $request->file('cover_image')->storeAs('/',$fileNameToStore,'public');

        }else{
            $fileNameToStore = 'noimage.jpg';
        }

        //Create Post
        $post =Post::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->cover_image = $fileNameToStore;
        $post->save();

        return redirect('/admin.posts')->with('success','Posts Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $post = Post::find($id);
        
        $post->delete();
        return redirect('/admin/posts')->with('success','Post Removed');
    }
}
