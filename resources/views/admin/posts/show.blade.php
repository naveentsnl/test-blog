@extends('layouts.admin')

@section('content')

	<div class="content-wrapper">

		<section class="content-header">
			<h1>
		    	Post Detail of {{ $post->name }}
		        <small>Control panel</small>
		    </h1>
			
		</section>


		<div class="well">
						
			<div class="row">

				<div class="col-sm-12 col-md-12">
					<a href="/admin/posts" class="btn btn-default">Go Back</a>

		            <h1>{{$post->title}}</h1>  
				    <img style="width:100%" src="/uploads/{{$post->cover_image}}">  
				    <div>
				        {!!$post->body!!}
				        <!-- {{$post->body}} show the content without style like tag. but {!!$post->body!!}  shows the style too-->
				    </div>
				    <small>
				        Written on {{$post->created_at}}
				    </small>
				    <hr>
				   
				            <a href="/posts/{{$post->id}}/edit" class="btn btn-default">Edit</a>

				            {!!Form::open(['action' => ['PostController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
				                {!!Form::hidden('_method','DELETE')!!}
				                {!!Form::submit('delete',['class'=> 'btn btn-danger'])!!}
				            {!!Form::close()!!}
				            
				     
	          </div>								
			</div>							
		</div>
	</div>
@endsection