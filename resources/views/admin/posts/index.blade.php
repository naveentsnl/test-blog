@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <h1>
                Posts  List
                <small>Control panel</small>
            </h1>
            
        </section>
        <div class="well">
                        
            <div class="row">

                <div class="col-sm-10 col-md-10">

                    @if (count($posts)>0)
                        @foreach ($posts as $post)
                                    <div class="col-sm-4 col-md-4">
                                        <img style="width:100%" src="/uploads/{{$post->cover_image}}">
                                    </div>
                                    <div class="col-sm-8 col-md-8 post" data-postid="{{ $post->id }}"> 
                                        <h3><a href="/admin_post/{{$post->id}}">{{$post->title}}</a></h3>
                                        <small>Written on {{$post->created_at}}</small>
                                        <small> By {{$post->user->name}}</small>      

                                    </div>        
                        @endforeach
                        {{$posts->links()}}
                        
                    @else
                        <h3>No posts found1</h3>        
                    @endif
                </div>
            </div>
        </div>
        
    </div>
        

@endsection



