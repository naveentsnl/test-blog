@extends('layouts.admin')

@section('content')
    <h1>Posts List</h1>

        @if (count($posts)>0)
            @foreach ($posts as $post)
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <img style="width:100%" src="/uploads/{{$post->cover_image}}">
                        </div>
                        <div class="col-sm-8 col-md-8 post" data-postid="{{ $post->id }}"> 
                            <h3><a href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
                            <small>Written on {{$post->created_at}}</small>
                            <small> By {{$post->user->name}}</small>      

                        </div>
                    </div>
                    
                </div>            
            @endforeach
            {{$posts->links()}}
            
        @else
            <h3>No posts found</h3>        
        @endif
    </div>
        

@endsection








