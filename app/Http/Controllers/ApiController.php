<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Post;
use App\Like;
use App\Comment;
use App\User;
use DB;
use Socialite;
use \Mail;

use App\Http\Resources\BlogResource;
use App\Http\Requests;


class ApiController extends Controller
{

    /**

    * Display a listing of the resource.
    
    *
    
    * @return \Illuminate\Http\Response
    
    */

    public function index()
    {

        //$posts = Post::orderBy('created_at','asc');

        $posts = DB::table('posts')->orderBy('created_at','asc')->get()->toJson();
        
        //return view('posts.index')->with('posts',$posts);

        return $posts;

    }

    /**

    * Show the form for creating a new resource.

    *

    * @return \Illuminate\Http\Response

    */

    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     *  The json will be the response with data and function if it success
     */
    
    public function store(Request $request)
    {
        
        //Get the token
        
        $token = User::where('id',$request->id)->pluck('api_token')->first();
        
        //dd($token);
       
        $this->validate($request,[
        
            'id' => 'required|exists:users,id,api_token,'.$request->token,
        
            'title' => 'required | min:3 | max:255',
        
            'body' => 'required|min:5',
        
            'cover_image' => 'image|nullable|max:2999|mimes:jpeg,bmp,png,jpg',
        
            'token' => 'required',
        ],
        
        [
            'id.exists' => 'Not available',
        ]);
        //dd('nav');
      
        //Get User id

        $user_id = User::where('api_token',$token)->pluck('id')->first();

        //dd($request->id);

        //dd($token);

        if($token == $request->input('token') & $user_id == $request->id){

            //Handle File Upload
            if($request->hasFile('cover_image')){

                //Get file name with extension
                $fileNameWithExt = $request ->file('cover_image')->getClientOriginalName();

                //Get the file name only
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                //Get the file extension only
                $extension = $request->file('cover_image')->getClientOriginalExtension();

                //Filename to store
                $fileNameToStore = $fileName.'_'.time().'.'.$extension;

                //Upload Image
                $path = $request->file('cover_image')->storeAs('/',$fileNameToStore,'public');

                }

                else{
                    $fileNameToStore = 'noimage.jpg';
                }

                //Create Post
                $post = new Post;

                $post->title = $request->title;

                $post->body = $request->body;

                $post->user_id = $request->id;

                $post->cover_image = $fileNameToStore;

                $post->api_token = $request->token;

                $post->save();

                return response()->json([

                    'post_id' =>  $post->id,

                    'title'         => $request->title,

                    'body'        => $request->body,

                    'api_token' => $request->token,

                    'created_time' => $post->created_at,

                ]);

        }

        else{

            return response()->json([
                "error" => "token and User id not mactched",
            ]);
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response

     * The json will be the response with data and function if it success
     */

    public function show($id)
    {
    
        $post = Post::find($id);

        if($post){

            //Show the Like, DisLike and Comment

            $like_post = Post::find($id);

            $comment = Comment::where(['post_id' => $id])->orderBy('created_at','asc')->get();

            $like_count = Like::where(['post_id' => $like_post->id])->count();

            return response()->json([

                'post_id' =>  $post->id,

                'title'         => $post->title,

                'body'        => $post->body,

                'api_token' => $post->api_token,

                'created_time' => $post->created_at,

                'updated_time' => $post->updated_at,

            ]);

        }
        else{

            return response()->json([
              
                "error" => "Not Found id",
            ]);

        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response

     * The json will be the response with data and function if it success and it will update
     */

    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * The json will be the response with data and function if it success and it will update
     */

    public function update(Request $request, $id)
    {

        //Get the token
        $token = User::where('id',$request->user_id)->pluck('api_token')->first();
        
        //dd($request->token);
       
        $this->validate($request,[
            
            'user_id' =>  'required|exists:users,id,api_token,'.$request->token,
            
            'title' => 'required | min:3 | max:255',
            
            'body' => 'required|min:5',
            
            'cover_image' => 'image|nullable|max:2999|mimes:jpeg,bmp,png,jpg',
            
            'token' => 'required'
        ],
        
        [
            'id.exists' => 'Not available',
        ]);
        //dd('nav');
        
        //Get User id
        $user_id = User::where('api_token',$token)->pluck('id')->first();

        //dd($request->id);

        //dd($token);

        if($token == $request->input('token') & $user_id == $request->user_id){

            //Handle File Upload
            if($request->hasFile('cover_image')){
                
                //Get file name with extension
                $fileNameWithExt = $request ->file('cover_image')->getClientOriginalName();

                //Get the file name only
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                //Get the file extension only
                $extension = $request->file('cover_image')->getClientOriginalExtension();

                //Filename to store
                $fileNameToStore = $fileName.'_'.time().'.'.$extension;

                //Upload Image
                $path = $request->file('cover_image')->storeAs('/',$fileNameToStore,'public');

            }
            else{
                $fileNameToStore = 'noimage.jpg';
            }

            //Create Post
            $post = Post::find($id);
               
            $post->title = $request->title;
                
            $post->body = $request->body;
                
            $post->user_id = $request->user_id;
                
            $post->cover_image = $fileNameToStore;
                
            $post->api_token = $request->token;
                
            $post->save();


            return response()->json([

                'post_id' =>  $post->id,

                'user_id' => $post->user_id,

                'title'         => $request->title,

                'body'        => $request->body,

                'api_token' => $request->token,

                'created_time' => $post->created_at,

            ]);

        }

        else{

            return response()->json([

                "error" => "token and User id not mactched",
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response

     * The json will be the response with data and function if it success
     */

    public function destroy(Request $request,$id)
    {

        //Get the token
        $token = User::where('id',$request->user_id)->pluck('api_token')->first();
        //dd($token);

        $user_id = User::where('api_token',$token)->pluck('id')->first();
        //dd($request->user_id);

        $post = Post::find($id);

        if($post){

            if($user_id==$request->user_id){

                if($post->cover_image != 'noimage.jpg'){

                    Storage::delete('/'.$post->cover_image,'public');
                }

                $post->delete();
                
                return response()->json([
                
                    "deleted" => $id,
                ]);

            }

            else{

                return response()->json([

                    "error" => "No match found",
                ]);
            }         

        }
        else{

            return response()->json([

                "error" => "No id found",
            ]);
        }
    }
}
