@extends('layouts.admin')

@section('content')

	<div class="content-wrapper">

		<section class="content-header">
			<h1>
		    	User Detail of {{ $user->name }}
		        <small>Control panel</small>
		    </h1>
			
		</section>


		<div class="well">
						
			<div class="row">

				<div class="col-sm-12 col-md-12">
					<a href="/users" class="btn btn-default">Go Back</a>

		            <div class="box-body">
		              <table class="table table-bordered">
		                <tr>
		                  	<th>Details</th>
		                  	<th>User Data</th>
		                </tr>
		             	<tr>
		             		<td> Name</td>
		             		<td>{{ $user->name }}</td>	
		             	</tr>

		             	<tr>
		             		<td>Email</td>
		             		<td>{{ $user->email }}</td>
		             	</tr>	

		             	<tr>
		             		<td>Created At</td>
		             		<td>{{ $user->created_at }}</td>
		             	</tr>	

		             	<tr>
		             		<td> <a href="/users/{{$user->id}}/edit" class="btn btn-primary">Edit</a></td>
		             	</tr>	
		             	<tr>
		             		<td>
		             			{!!Form::open(['action' => ['UsersController@destroy', $user->id], 'method' => 'POST'])!!}
                				
                				{!!Form::hidden('_method','DELETE')!!}
                				
                				{!!Form::submit('delete',['class'=> 'btn btn-danger'])!!}
            					{!!Form::close()!!}
            				</td>
		             	</tr>
		             					
									
		              </table>
		        
		            </div>
	          </div>								
			</div>							
		</div>
	</div>
@endsection