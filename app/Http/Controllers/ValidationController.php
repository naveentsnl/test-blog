<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class ValidationController extends Controller
{
	/**

	* @method validation_show

	* To display the validation page

	* Created by Naveen TSNL

	**/
    public function validation_show(){

    	//Returns the view page

    	return view('new.validation');

    }

    /**

	* @method validation

	* To make the validation

	* @parem Request $request

	* Getting Checkin and Checkout time

	* @return the 1 while success else return the custom error

	* Created by Naveen TSNL

	**/

    public function validation(Request $request){

    	//Validation

    	//Checks the checkin time and checkout time

    	$timestamp = date(DATE_ATOM, time() + (1 * 60 * 60));

    	$dates = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, 'Asia/Kolkata');

    	dd($dates);

    	dd(date_default_timezone_get());

    	$this->validate($request,[

    		'checkin' => 'required|date_format:Y-m-d H:i:s|after:'.date(DATE_ATOM, time() + (1 * 60 * 60)),

    		'checkout' => 'required|date_format:Y-m-d H:i:s|after_or_equal:checkin-date',


    	],

    	[

    		'checkin.required' => 'Must enter the checkin date',

    		'checkout.required' => 'Check out date is important',

    		'checkout.after_or_equal' => 'Check Out Time required greater than from check in time',

    		'checkin.after' => 'Check In Time required greater than 1 hour from now',


    	]);

    	return 'success';

    }
}
