@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <h1>
                Create Post
                <small>Control panel</small>
            </h1>
            
        </section>


        <div class="well">
                        
            <div class="row">

                <div class="col-sm-12 col-md-12">

                    
                    {{ Form::open(['action'=> 'AdminPostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        <div class="form-group">
                            {{Form::label('title','Title')}}
                            {{Form::text('title','',['class' => 'form-control', 'placeholder' => 'Title'])}}
                        </div> 
                        <div class="form-group">
                            {{Form::label('body','Body')}}
                            {{Form::textarea('body','',['id'=>'article-ckeditor','class' => 'form-control', 'placeholder' => 'Body Content'])}}
                        </div>
                        <div class="form-group">
                            {{Form::file('cover_image')}}
                        </div>
                        {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
                    {{ Form::close() }}
              </div>                                
            </div>                          
        </div>
    </div>
@endsection