@extends('layouts.admin')

@section('content')

	<div class="content-wrapper">

		<section class="content-header">
			<h1>
		    	Users List
		        <small>Control panel</small>
		    </h1>
			
		</section>

		<div class="well">
						
			<div class="row">

				<div class="col-sm-12 col-md-12">

					<?php $no = 0; ?>
						 <!-- /.box-header -->
		            <div class="box-body">
		              <table class="table table-bordered">
		                <tr>
		                  	
		                  	<th>SNo</th>
		                  	<th>Name</th>
		                  	<th>Email</th>
		                  	<th>Created at</th>
		                </tr>
		                @if(count($users)>0)

							@foreach($users as $user)
				                <tr>
				                	<td>{{ ++$no }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->created_at }}</td>
									<td><a href="/users/{{ $user->id}}" class="btn btn-info">View</a></td>
									<td><a href="#" class="btn btn-primary">Edit</a></td>
									<td><a href="#" class="btn btn-danger">Delete</a></td>
				                </tr>
				             @endforeach
		              </table>
		              {{$users->links()}}
		            </div>
		             
		            @else
		            	<h3>No Users found</h3>
		            @endif
	          </div>

								
			</div>							
		</div>
	</div>
		

@endsection