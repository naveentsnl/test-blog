<!DOCTYPE html>
<html>
<head>

	<title>Checkin Validation</title>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.14/moment-timezone.min.js"></script>
	<script type="text/javascript">
		var timezone_offset_minutes = new Date().getTimezoneOffset();
		timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

		// Timezone difference in minutes such as 330 or -360 or 0
		console.log(timezone_offset_minutes); 
	</script>

</head>
<body>

	<h1>Checkin and Checkout Validation</h1>

	<br>

	<form action="/valid_check" method="POST">

		{{ csrf_field() }}
		
		<input type="datetime" name="checkin">
		<br><br>

		<input type="datetime" name="checkout">
		<br><br>

		<input type="submit" name="Submit">

		<input type="hidden" name="tz" id="tz">

	</form>

	@foreach ($errors->all() as $error)
    	
    	<li>{{ $error }}</li>

	@endforeach 


</body>
</html>