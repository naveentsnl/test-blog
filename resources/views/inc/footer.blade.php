<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2019-2025 <a href="https://codegama.com/">Codegama</a>.</strong> All rights
    reserved.
  </footer>