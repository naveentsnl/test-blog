<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//This is Normal

/*Route::get('/', function () {
    return view('welcome');
});
*/


//By Create new controller it can work
//To Create controller in terminal
// $php artisan make:controller PageController
Route::get('/', 'PagesController@index');
Route::get('/services', 'PagesController@services');
Route::get('/about','PagesController@about');


Route::get('/home', function () {
    return view('welcome');
});
Route::get('helloworld', function () {
    return '<h1>Hello World</h1>';
}); 
/*Route::get('/about',function(){
    return view('pages.about');
});

Route::get('/user/{id}',function($id){
    return 'The user id is '.$id;
});


Route::get('/user/{id}/{name}',function($id,$name){
    return 'The user id is '.$id. ' for the name '. $name;
});
*/

Route::resource('posts','PostController');
Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
Route::post('/like', 'PostController@likePost')->name('like');
//Route::post('/comment', 'CommentController@store')->name('comment');



Route::get('/like/{id}', 'PostController@like');
Route::post('/comment/{id}', 'PostController@comment');


Route::get('valid', 'ValidationController@validation_show');
Route::post('valid_check', 'ValidationController@validation');

Route::get('/email', 'PostController@email');

//Route::get('users/{{apitoken}}','AuthController@user');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

 Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
 Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');

//Route::view('/admin', 'admin');
Route::view('/admin', 'admin.index');
Route::view('/admin2', 'admin.index2');
Route::view('/starter', 'admin.starter');

Route::resource('/users', 'UsersController');
Route::resource('/admin_post', 'AdminPostsController');


Route::view('/test', 'layouts.admin');
Route::view('/test1','inc.header');