@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <h1>
                User Detail of {{ $user->name }}
                <small>Control panel</small>
            </h1>
            
        </section>


        <div class="well">
                        
            <div class="row">

                <div class="col-sm-12 col-md-12">
                    
                    {{ Form::open(['action'=> ['UsersController@update', $user->id], 'method' => 'POST']) }}
                        <div class="form-group">
                            {{Form::label('name','Name')}}
                            {{Form::text('name',$user->name,['class' => 'form-control', 'placeholder' => 'Name'])}}
                        </div> 
                        <div class="form-group">
                            {{Form::label('email','Email')}}
                            {{Form::text('email',$user->email,['class' => 'form-control', 'placeholder' => 'Email Address'])}}
                        </div>

                        <div class="form-group">
                            {{Form::label('password','Password')}}
                            {{Form::password('password',['class' => 'form-control', 'placeholder' => 'Password'])}}
                        </div>

                        <div class="form-group">
                            {{Form::label('cpassword','Confirm Password')}}
                            {{Form::password('cpassword',['class' => 'form-control', 'placeholder' => 'Confirm Password'])}}
                        </div>
                        
                        {{Form::hiddeN('_method','PUT')}}
                        {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
                    {{ Form::close() }}
              </div>                                
            </div>                          
        </div>
    </div>
@endsection